# Javascript
## Lorenz Glißmann

---

## Ablauf

- Basics (30')
- Concepts (30')
- Practice (30')

---

### Basics

- basic types & operations
- arrays & objects
- control flow
- functions

----

#### basic types & operations

----

#### arrays & objects

----

#### control flow

----

#### functions

---

### Concepts

- JSON
- higher order functions
- functions as constructors
- prototypes
- event loop
- concurrency (Nebenläufigkeit)

----

#### JSON

----

#### higher order functions

----

#### functions as constructors

----

#### prototypes

----

#### event loop

----

#### concurrency (Nebenläufigkeit)

---

## Javascript ecosystem

- node vs. browser
- npm/yarn
- electron
- Bundler (Webpack & Co)
- Frameworks

---

## modern Javascript

- `==` vs `===`
- `var`, `let`, `const`
- Typescript

---

## Sources

- inspiration for the basics: https://learnxinyminutes.com/docs/javascript/
